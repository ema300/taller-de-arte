//var opciones = ['florida,buenos aires','flores, buenos aires','los polvorines,buenos aires','carapachay,buenos aires','don torcuato,buenos aires', 'malvinas argentinas,buenos aires'];

var listaDirecciones =  []; 
var miArreglo =  [];  // arreglo auxiliar
var cadena1 = "";     // strings auxiliares
var cadena2 = "";
var cadena3 = "";
var i=0;

  function configureAjaxCalls(direccion) {

      fetch('http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion= ' + direccion) //ingreso y coneccion con el servidor
        .then(ajaxPositive)
        .catch(showError);
        
    function ajaxPositive(response) {
      console.log('response.ok: ', response.ok);
      if(response.ok) {
        response.text().then(showResult);
      } else {
        showError('status code: ' + response.status);
      }
    }

    function showResult(txt) {  
      var respuesta = txt;

      //alert ('direccion ingresada: ' + document.getElementById('direccion').value)

      var pos = respuesta.indexOf('errorMessage');

      if (pos == -1 ){ // es correcta
        let myRe1= new RegExp(/(\"direccion\": \")[a-zA-Z0-9áéíóúñ .\\]*/g) ;
        let myRe2 = new RegExp(/(d\": \")[a-zA-Z0-9áéíóúñ. \\]*/g) ;
        
        txt = txt.replace(/\\u00e1/g,'á');  // la consulta no devuelve caracteres especiales, hay que convertir
        txt = txt.replace(/\\u00e9/g,'é'); //reemplazar
        txt = txt.replace(/\\u00ed/g,'í');
        txt = txt.replace(/\\u00f3/g,'ó');
        txt = txt.replace(/\\u00fa/g,'ú');
        txt = txt.replace(/\\u00f1/g,'ñ');

        listaDirecciones = txt.match(myRe1) ; //voy guardando en la lista de direcciones
        miArreglo = txt.match(myRe2) ;
    
        for (i=0;i<listaDirecciones.length;i++) {    // armo una cadena de caracteres
          cadena1 = listaDirecciones[i].slice(14);
          cadena2 = miArreglo[i].slice(5);
          cadena1 = cadena3.concat(cadena1,", ");
          cadena3 = "";
          cadena1 = cadena3.concat(cadena1,cadena2);
          listaDirecciones[i]= cadena1
        } 

        //return listaDirecciones;
        
      } else {           
        listaDirecciones = ['Dirección incorrecta'];

      }

    }

    function showError(err) { 
      listaDirecciones = ['Error en el proceso de verificacion'];
    }
  }

//////////////////////mostrar direcciones////////////////////////

$( document ).ready(function() { //lee este mismo documento
  var items = [];
  var listaDesplegable = $('#opciones');
  var divNone = $("#direccion").next();
  $("#direccion").keyup(function() {
    $(this).empty();
    listaDesplegable.empty();
    divNone.empty();
    items = [];
    valor = $(this).val().toLowerCase();

    configureAjaxCalls(valor);

    $(listaDirecciones).filter(function (i,n){
      if(valor.length != 0 && (n === 'Error en el proceso de verificacion' ||	n === 'Dirección incorrecta')){
        divNone.css('display','inline-block');
        divNone.append("<p style='color:red'> Direccion no encontrada</p>");
      }else{
        if(valor.length != 0 ){
          items.push(n);
        }
        
      }


    });

    for (var i = 0; i < items.length; i++) {
      item = items[i];
      listaDesplegable.append("<option value='" + item+ "'>"); // genera las opciones en el main
    }

  });

});
  ////////////////////////////// fUNCIONES DE INSCRIPCION A TALLERES ////////////////////////7

function inscribirTaller(){

  alert("Solicitud Recibida! Aguardando aprobación");
}

//////////////////////////////////////////////////////////////////////////////////////////